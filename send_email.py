# -*- coding: utf-8 -*-
"""
Usage:
    import email
    email.send(from_=<string>,
                  to=<list>,
                  subject=<string>,
                  text=<string>, files=<list>)
"""

import smtplib

from os.path import basename

from email.mime.application import MIMEApplication

from email.mime.multipart import MIMEMultipart

from email.mime.text import MIMEText

from email.utils import COMMASPACE, formatdate

import sys

from decouple import config


PASSWORD = config('PASSWORD')


def arg_is_list(to):
    return isinstance(to, list)


def load_attachment(files, msg):
    for f in files or []:
                with open(f, "rb") as fil:
                    part = MIMEApplication(fil.read(), Name=basename(f))
                    part['Content-Disposition'] = 'attachment; filename="%s"' \
                                                  % basename(f)
                    msg.attach(part)


def send_email(from_,to,subject,text,files=None):
    # validate arguments e load values
    if arg_is_list(to):
        msg = MIMEMultipart()
        msg['From'] = from_
        msg['To'] = COMMASPACE.join(to)
        msg['Date'] = formatdate(localtime=True)
        msg['Subject'] = subject
        msg.attach(MIMEText(text))

    else:
        print('Error: to must be list type')
        sys.exit()

    
    # load attachment
    load_attachment(files, msg)
        

    # conect to email server
    smtp = smtplib.SMTP('smtp.gmail.com', 587)        
    smtp.starttls()
    smtp.login(from_, PASSWORD)


    # send email
    smtp.sendmail(from_, to, msg.as_string())


    # close server connection
    smtp.close()

